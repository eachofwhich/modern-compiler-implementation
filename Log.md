# Modern Compiler Implementation in ML
===


## Lexical Analysis
===
A lexical specification should be *complete*, always matching some prefix of the input string.
We can use a rule with the lowest priority that matches any character and reports an error.
((why?))

To disambiguate, we use these rules, in order:
-   The regex that matches the longest prefix will produce the next token
-   The regex with the higher priority will produxe the next token. The order of regexes in 
    the specification is significant


## Parsing
===
((abbreviationgs don't add expressive power))
((recursion does))
((one of the non-terminals is distinguished as the start symbol of the grammar))
((precedence binds around operators, associativity groups when all opeartors bind equally))
((need an eof marker))


## Inbox
===

