
module TreeMod = struct
    type ('a, 'b) pair = ('a * 'b)
    type ('a, 'b) t = 
        | Leaf
        | Tree of ('a, 'b) pair * ('a, 'b) t * ('a, 'b) t

    let empty = Leaf

    let rec insert k v = function
        | Leaf                          -> Tree ((k, v), Leaf, Leaf)
        | Tree ((kk, vv), left, right) -> if k > kk
                                          then Tree ((kk, vv), left, insert k v right)
                                          else Tree ((kk, vv), insert k v left, right)

    let rec member k = function
        | Leaf                        -> false
        | Tree ((kk, _), left, right) -> if k < kk then member k left
                                         else if k > kk then member k right
                                         else true

    let rec lookup k = function 
        | Leaf                         -> None
        | Tree ((kk, vv), left, right) -> if k = kk then Some vv
                                          else if k < kk then lookup k left
                                          else lookup k right
end