open Core_kernel

type id = string
type binop = Plus | Minus | Times | Div

type stm = 
    | CompoundStm of stm * stm
    | AssignStm   of id * exp
    | PrintStm    of exp list
and
    exp =
    | IdExp     of id
    | NumExp    of int
    | OpExp     of exp * binop * exp
    | EseqExp   of stm * exp


let prog = 
    CompoundStm(
        AssignStm("a", OpExp(NumExp 5, Plus, NumExp 3)),
        CompoundStm(
            AssignStm("b",
                      EseqExp(
                          PrintStm [IdExp "a"; OpExp (IdExp "a", Minus, NumExp 1)],
                          OpExp(NumExp 10, Times, IdExp "a"))),
            PrintStm[IdExp "b"]))


(** 
Write an ML function (maxargs : stm -> int) that tells the maximum number of arguments
of any `print` statement within any subexpression of a given statement.
*)
let rec walk_stm = function
    | CompoundStm (s1, s2) -> Int.max (walk_stm s1) (walk_stm s2)
    | AssignStm   (_, e)   -> walk_exp e
    | PrintStm    (es)     -> let so_far = List.length es in
                              List.fold es 
                                        ~init:so_far 
                                        ~f:(fun acc e -> Int.max acc (walk_exp e))


and walk_exp = function
    | IdExp _ | NumExp _ -> 0
    | OpExp (e1, _, e2)  -> Int.max (walk_exp e1) (walk_exp e2)
    | EseqExp (s, e)     -> Int.max (walk_stm s) (walk_exp e)
                                

let maxargs = walk_stm


let _ = 
    assert(2 = (maxargs prog))


(**
Write an ML function interp : stm -> unit that interprets a program in this language.
To write in a functional style - without assignment or arrays - maintain a list of
(variable, integer) pars, and produce new versions of this list at each AssignStm.
*)
module Env = struct
    type ('a, 'b) t = ('a * 'b) list
    let empty = []
    let set env symbol value = (symbol, value) :: env
    let get env symbol = 
        match List.Assoc.find env ~equal:String.equal symbol with
            | Some(n) -> n
            | None    -> failwith (Printf.sprintf "Unknown reference %s" symbol)
end


let rec eval_stm env = function
    | CompoundStm (s1, s2) -> eval_stm (eval_stm env s1) s2
    | AssignStm (id, e)    -> let (n, _) = eval_exp env e in Env.set env id n
    | PrintStm (es)        -> eval_exp_sequence env es


and eval_exp env = function
    | IdExp id           -> (Env.get env id, env)
    | NumExp n           -> (n, env)
    | OpExp (e1, op, e2) -> eval_infix_exp env e1 op e2
    | EseqExp (s, e)     -> let env2 = eval_stm env s in
                            eval_exp env2 e


and eval_exp_sequence env es =
    List.fold es
            ~init:env
            ~f:(fun next_env e -> 
                    let (n, next_next_env) = eval_exp next_env e in 
                    Printf.printf "%i" n;
                    next_next_env)


and eval_infix_exp env e1 op e2 = 
    let (n1, env1) = eval_exp env e1 in 
    let (n2, env2) = eval_exp env1 e2 in
    match op with
        | Plus  -> (n1 + n2, env2)
        | Minus -> (n1 - n2, env2)
        | Times -> (n1 * n2, env2)
        | Div   -> (n1 / n2, env2)


let _ =
    let env = eval_stm Env.empty prog in 
    let a = List.Assoc.find env ~equal:String.equal "a" in
    let b = List.Assoc.find env ~equal:String.equal "b" in
    match (a, b) with
        | (Some(8), Some(80)) -> ()
        | _                   -> failwith "eval is broken"
