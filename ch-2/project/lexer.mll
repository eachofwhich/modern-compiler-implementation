
{
  open Token

  let get = Lexing.lexeme
  let comment_level = ref 0

  let make_location lexbuf =
    let p = lexbuf.Lexing.lex_curr_p in
    let file = p.pos_fname in
    let line = p.pos_lnum in 
    let column = p.pos_cnum - p.pos_bol in
    { file; line; column }

  exception SyntaxError of string * location
}



let id = ['_' 'a'-'z' 'A'-'Z'] ['_' '0'-'9' 'a'-'z' 'A'-'Z']*
let int = ['0'-'9']+
let newline = '\n' | '\r' | "\r\n"
let whitespace = [' ' '\t']+


rule program = parse
  | "array"     { Array (make_location lexbuf) }
  | "break"     { Break (make_location lexbuf) }
  | "do"        { Do (make_location lexbuf) }
  | "end"       { End (make_location lexbuf) }
  | "false"     { False (make_location lexbuf) }
  | "for"       { For (make_location lexbuf) }
  | "function"  { Function (make_location lexbuf) }
  | "if"        { If (make_location lexbuf) }
  | "in"        { In (make_location lexbuf) }
  | "let"       { Let (make_location lexbuf) }
  | "nil"       { Nil (make_location lexbuf) }
  | "of"        { Of (make_location lexbuf) }
  | "then"      { Then (make_location lexbuf) }
  | "to"        { To (make_location lexbuf) }
  | "true"      { True (make_location lexbuf) }
  | "type"      { Type (make_location lexbuf) }
  | "var"       { Var (make_location lexbuf) }
  | "while"     { While (make_location lexbuf) }

  | id          { Id (get lexbuf, make_location lexbuf) }
  | "\""        { string (Buffer.create 100) lexbuf }
  | int         { Int (int_of_string (get lexbuf), make_location lexbuf) }
  | whitespace  { program lexbuf }
  | newline     { Lexing.new_line lexbuf; program lexbuf }

  | "/*"        { comment lexbuf }

  | "<>"        { Ne (make_location lexbuf) }
  | "="         { Eq (make_location lexbuf) }
  | "<"         { Lt (make_location lexbuf) }
  | ">"         { Gt (make_location lexbuf) }
  | "<="        { LtEq (make_location lexbuf) }
  | ">="        { GtEq (make_location lexbuf) }
  | "&"         { Amp (make_location lexbuf) }
  | "|"         { Pipe (make_location lexbuf) }
  | ":="        { ColonEq (make_location lexbuf) }

  | '+'         { Plus (make_location lexbuf) }
  | '-'         { Minus (make_location lexbuf) }
  | '*'         { Mul (make_location lexbuf) }
  | '/'         { Div (make_location lexbuf) }
  | ';'         { Semi (make_location lexbuf) }
  | ':'         { Colon (make_location lexbuf) }
  | '('         { LParen (make_location lexbuf) }
  | ')'         { RParen (make_location lexbuf) }
  | '['         { LBracket (make_location lexbuf) }
  | ']'         { RBracket (make_location lexbuf) }
  | '{'         { LBrace (make_location lexbuf) }
  | '}'         { RBrace (make_location lexbuf) }
  | ','         { Comma (make_location lexbuf) }
  | '.'         { Dot (make_location lexbuf) }
  | eof         { Eof }
  | _ 
      { raise (SyntaxError 
                ("Unexpected character: " ^ get lexbuf,
                make_location lexbuf))
      }

and string buf = parse
  (* For multiline strings, we capture the linebreaks. *)
  |  newline   { 
      Buffer.add_string buf (get lexbuf); 
      Lexing.new_line lexbuf;
      string buf lexbuf 
    }
  | '"'            { String(Buffer.contents buf, make_location lexbuf) }
  | '\\' 'n'       { Buffer.add_char buf '\n'; string buf lexbuf }
  | '\\' 'b'       { Buffer.add_char buf '\b'; string buf lexbuf }
  | '\\' 't'       { Buffer.add_char buf '\t'; string buf lexbuf }
  | '\\' '\\'      { Buffer.add_char buf '\\'; string buf lexbuf }
  | [^ '"' '\\']   { Buffer.add_string buf (get lexbuf); string buf lexbuf }
  | eof            { raise (SyntaxError ("Unterminated string", make_location lexbuf)) }
  | _              { 
      raise (SyntaxError 
              ("Unexpected character in string: " ^ get lexbuf,
              make_location lexbuf))
    }


and comment = parse
  | newline     { Lexing.new_line lexbuf; comment lexbuf }
  | "\\*"       { comment_level := !comment_level + 1; comment lexbuf }
  | "*/"        { 
      comment_level := max 0 (!comment_level - 1); 
      if !comment_level = 0 then program lexbuf else comment lexbuf
    }
  | eof         { raise (SyntaxError ("Unterminated comment", make_location lexbuf)) }
  | _
      { comment lexbuf }


(*
-   Add filename
-   Use `as foo`?
*)