
  type location = {
    file   : string;
    line   : int;
    column : int
  }


  type token =
    (* Keybords *)
    | Array     of location
    | Break     of location
    | Do        of location
    | End       of location
    | False     of location
    | For       of location
    | Function  of location
    | If        of location
    | In        of location
    | Let       of location
    | Nil       of location
    | Of        of location
    | Then      of location
    | To        of location
    | True      of location
    | Type      of location
    | Var       of location
    | While     of location

    | Id        of string * location
    | String    of string * location
    | Int       of int    * location

    | Ne        of location
    | Eq        of location
    | Lt        of location
    | Gt        of location
    | LtEq      of location
    | GtEq      of location
    | Amp       of location
    | Pipe      of location
    | ColonEq   of location
    | Plus      of location
    | Minus     of location
    | Mul       of location
    | Div       of location
    | Semi      of location
    | Colon     of location
    | LParen    of location
    | RParen    of location
    | LBracket  of location
    | RBracket  of location
    | LBrace    of location
    | RBrace    of location
    | Comma     of location
    | Dot       of location
    | Eof


let string_of_token = function
    | Array (_)     -> "array"
    | Break (_)     -> "break"
    | Do (_)        -> "do"
    | End (_)       -> "end"
    | False (_)     -> "false"
    | For (_)       -> "for"
    | Function (_)  -> "function"
    | If (_)        -> "if"
    | In (_)        -> "in"
    | Let (_)       -> "let"
    | Nil (_)       -> "nil"
    | Of (_)        -> "of"
    | Then (_)      -> "then"
    | To (_)        -> "to"
    | True (_)      -> "true"
    | Type (_)      -> "type"
    | Var (_)       -> "var"
    | While (_)     -> "while"

    | Id (s, _)     -> s
    | String (s, _) -> s
    | Int (n, _)    -> string_of_int n
    | Ne (_)        -> "<>"
    | Eq (_)        -> "="
    | Lt (_)        -> "<"
    | Gt (_)        -> ">"
    | LtEq (_)      -> "<="
    | GtEq (_)      -> ">="
    | Amp  (_)      -> "&"
    | Pipe (_)      -> "|"
    | ColonEq (_)   -> ":="
    | Plus (_)      -> "+"
    | Minus (_)     -> "-"
    | Mul (_)       -> "*"
    | Div (_)       -> "/"
    | Semi (_)      -> ";"
    | Colon (_)     -> ":"
    | LParen (_)    -> "("
    | RParen (_)    -> ")"
    | LBracket (_)  -> "["
    | RBracket (_)  -> "]"
    | LBrace (_)    -> "{"
    | RBrace (_)    -> "}"
    | Comma (_)     -> ","
    | Dot (_)       -> "."
    | Eof           -> ""