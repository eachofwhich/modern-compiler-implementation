(* (~eb) do we need to open these? *)
open Core_kernel


let handle_lexing_error = function 
  | Lexer.SyntaxError (message, location) -> 
        failwith (Printf.sprintf "SyntaxError: %s in %s %d:%d"
                      message
                      location.file 
                      location.line 
                      location.column);
  | _ -> failwith "Unknown exception occurred while lexing"

let set_filename filename lexbuf =
  let open Lexing in
  lexbuf.lex_curr_p <- { lexbuf.lex_curr_p with pos_fname = filename }



let lex_tokens lexbuf =
  let rec loop () = match Lexer.program lexbuf with
    | exception e -> handle_lexing_error e
    | Token.Eof   -> []
    | t           -> t :: (loop ())
  in
    loop ()


let strings_of_tokens = 
  List.map ~f:Token.string_of_token


let lex =
  let lexbuf = Lexing.from_channel In_channel.stdin in
  set_filename "<stdin>" lexbuf;
  let tokens = lex_tokens lexbuf in
  let strings = strings_of_tokens tokens in
  Out_channel.output_string stdout (String.concat ~sep:" " strings)
